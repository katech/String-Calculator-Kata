package com.string.calculator;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DelimiterExpression {

    public static final String START_DELIMITER_CONTAINER = "[";
    public static final String END_DELIMITER_CONTAINER = "]";
    private String inputValue;

    public DelimiterExpression(final String inputValue){
        this.inputValue = inputValue;
    }

    public List<String> getSeparators() {
        if ("".equals(inputValue)) {
            return Collections.emptyList();
        }
        return Stream.of(inputValue.split(Pattern.quote(END_DELIMITER_CONTAINER + START_DELIMITER_CONTAINER)))
                .map(separator -> separator
                        .replace(START_DELIMITER_CONTAINER, "")
                        .replace(END_DELIMITER_CONTAINER, ""))
                .collect(Collectors.toList());
    }

}
