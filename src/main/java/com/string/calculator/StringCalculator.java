package com.string.calculator;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StringCalculator {


    /**
     *
     * @param inputValue
     * @return sum value
     */
    public int getSum(final String inputValue) throws NegativeNumberNotSupportedException {

        InputValueExpression inputValueExpression = new InputValueExpression(inputValue);

        if (inputValueExpression.isEmpty()) {
            return 0;
        }

        final List<Integer> listNumbers = inputValueExpression.getListNumbers();
        if (listNumbers.stream().anyMatch(isNegative())) {
            throw NegativeNumberNotSupportedException.create(getNegativeFrom(listNumbers));
        }
        return listNumbers
                .stream()
                .filter(number -> number <= 1000)
                .mapToInt(Integer::intValue)
                .sum();
    }

    private static List<Integer> getNegativeFrom(final List<Integer> numbers) {
        return numbers.stream().filter(isNegative()).collect(Collectors.toList());
    }
    private static Predicate<Integer> isNegative() {
        return number -> number < 0;
    }

}
