package com.string.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputValueExpression {

    private final static List<String> listOfDelimiters = Arrays.asList(",","\n");

    public static final String START_DELIMITER = "//";
    public static final String END_DELIMITER = "\n";

    private final String inputValue;

    public InputValueExpression(final String inputValue){
        this.inputValue = inputValue;
    }

    public boolean isEmpty(){
        return inputValue.isBlank();
    }

    private DelimiterExpression getDelimitersExpression(){
        if (!hasCustomSeparator()) {
            return new DelimiterExpression("");
        }
        return new DelimiterExpression(inputValue.substring(getBeginDelimiterIndex(), getEndDelimiterIndex()));
    }

    private boolean hasCustomSeparator() {
        return inputValue.startsWith(START_DELIMITER);
    }

    private int getEndDelimiterIndex() {
        return inputValue.indexOf(END_DELIMITER);
    }

    private int getBeginDelimiterIndex() {
        return inputValue.indexOf(START_DELIMITER) + START_DELIMITER.length();
    }

    private NumbersExpression getNumberExpression() {
        if (!hasCustomSeparator()) {
            return new NumbersExpression(inputValue);
        }
        return new NumbersExpression(inputValue.substring(getEndDelimiterIndex() + 1));
    }


    public List<Integer> getListNumbers() {
        return getNumberExpression().getNumbers(getListDelimiters());
    }

    private List<String> getListDelimiters(){
        final List<String> listDelimiters = new ArrayList<>();
        listDelimiters.addAll(listOfDelimiters);
        listDelimiters.addAll(getDelimitersExpression().getSeparators());
        return listDelimiters;
    }
}
