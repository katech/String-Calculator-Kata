package com.string.calculator;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;

public class StringCalculatorTest {

    private StringCalculator stringCalculator;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp(){
        stringCalculator = new StringCalculator();
    }

    @Test
    public void should_return_the_sum_when_given_two_numbers() throws Exception {
        assertEquals(stringCalculator.getSum("1,2"),3);
        assertEquals(stringCalculator.getSum("2,3"),5);
        assertEquals(stringCalculator.getSum("6,5"),11);
        assertEquals(stringCalculator.getSum("19,31"),50);
    }

    @Test
    public void add_given_empty_string_should_return_zero() throws Exception {
        assertEquals(stringCalculator.getSum("  "),0);
        assertEquals(stringCalculator.getSum(""),0);
    }

    @Test
    public void add_given_numbers_should_return_the_sum() throws Exception{
        assertEquals(stringCalculator.getSum("1,2,5"), 8);
        assertEquals(stringCalculator.getSum("5,5,5,5,5,5"), 30);
        assertEquals(stringCalculator.getSum("9,9,9,9,9,9"),54);
    }

    @Test
    public void add_given_numbers_separated_by_new_line_should_return_the_sum() throws Exception {
        assertEquals(stringCalculator.getSum("1\n2,3"),6);
        assertEquals(stringCalculator.getSum("1\n1\n1"),3);
    }

    @Test
    public void should_return_the_sum_when_given_specific_separator() throws Exception{
        assertEquals(stringCalculator.getSum("//;\n2;2"),4);
        assertEquals(stringCalculator.getSum("//*\n3*5"),8);
        assertEquals(stringCalculator.getSum("//#\n1#3"),4);
        assertEquals(stringCalculator.getSum("//#\n9"),9);
        assertEquals(stringCalculator.getSum("//#\n6#6#7"),19);
    }

    @Test
    public void should_throw_exception_when_given_one_negative_number_() throws Exception {
        expectedException.expect(NegativeNumberNotSupportedException.class);
        expectedException.expectMessage("Negative  value not supported: -1");
        stringCalculator.getSum("//;\n2;-1");
    }

    @Test
    public void should_throw_exception_when_given_several_negative_numbers() throws Exception {
        expectedException.expect(NegativeNumberNotSupportedException.class);
        expectedException.expectMessage("Negative  value not supported: -2, -1");
        stringCalculator.getSum("//;\n-2;-1");
    }

    @Test
    public void add_should_ignore_numbers_greater_than_one_thousand() throws Exception {
        assertEquals(stringCalculator.getSum("2,1001"), 2);
    }

}
